
import UIKit


class ViewController: UITableViewController , UISearchControllerDelegate, UISearchBarDelegate{
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 400, height: 20))
    var apiServices = UserViewModel()
    var userinfo : UserInfo?        // Initial API response
    var userdetails : UserDetails?  // Repo url response
    var searchItem = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCells()
        setUpUI()
        //apiServices.fetchData(myparameter: #""""#)
        apiServices.fetchDataOfUsers2(searchBarText: #""""#) { userInfo in
            self.userinfo = userInfo
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func setUpUI(){
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9568627451, green: 0.4980392157, blue: 0.5450980392, alpha: 1)
        self.navigationItem.title = "Home"
        self.navigationController?.navigationBar.tintColor = UIColor(white: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white];
        let search = UISearchController(searchResultsController: nil)
        search.delegate = self
        search.searchBar.delegate = self
        search.obscuresBackgroundDuringPresentation = true
        search.searchBar.backgroundColor =  #colorLiteral(red: 0.9568627477, green: 0.4980392157, blue: 0.5450980663, alpha: 1)
        search.searchBar.searchTextField.leftView?.tintColor = .white
        search.searchBar.searchTextField.tokenBackgroundColor = .white
        search.hidesNavigationBarDuringPresentation = true
        search.searchBar.searchBarStyle = .prominent
        self.navigationItem.searchController = search
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchItem = searchText
        
//        if searchText != ""{
//            self.apiServices.fetchData(myparameter: #"""# + searchText + #"""#)
//        }else{
//            apiServices.fetchData(myparameter: #""""#)
//        }
        
        self.userinfo?.items?.removeAll()
        self.apiServices.fetchDataOfUsers2(searchBarText: searchText) { userInfo in
            self.userinfo = userInfo
            if searchText.count > 0 && self.userinfo?.items?.isEmpty == true{
                self.presentAlert()
            }
            else{
                self.apiServices.fetchDataOfUsers2(searchBarText: #""""#) { userInfo in
                    self.userinfo = userInfo
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userinfo?.total_count == nil{
            return 0
        }else{
            return userinfo?.items?.count ?? 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell",
                                                 for: indexPath) as! UserViewCell
       
        cell.viewDetailButton.isHidden = false
        cell.imgOutlet.layer.borderWidth = 1.0
        cell.imgOutlet.layer.masksToBounds = false
        cell.imgOutlet.layer.borderColor = UIColor.white.cgColor
        cell.imgOutlet.layer.cornerRadius = 35
        cell.imgOutlet.clipsToBounds = true
        
        
        if let safename = userinfo?.items?[indexPath.row].login {
            cell.nameoutlet.text = safename
        }
        
        if let safescore = userinfo?.items?[indexPath.row].score{
            cell.scoreoutlet.text = "Score : \(safescore)"
        }
        
        if let safeimg = userinfo?.items?[indexPath.row].avatar_url{
            var safemyimg = URL(string: safeimg) ?? URL(string: "https://avatars.githubusercontent.com/u/499550?v=4")
            cell.imgOutlet.load(url: safemyimg!)
        }
        
        cell.viewDetailButton.tag = indexPath.row
        cell.viewDetailButton.addTarget(self, action: #selector(ViewDetailClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func ViewDetailClicked(_ sender: UIButton){
        guard let repoUrl = userinfo?.items?[sender.tag].repos_url else{
            return
        }
        if let vc = storyboard?.instantiateViewController(identifier: "Profile") as? ProfileViewController{
            vc.loginName = userinfo?.items![sender.tag].login
            if let data = userinfo?.items?[sender.tag].avatar_url{
                vc.avatarStr = data
            }
            vc.repo = repoUrl
            vc.userRepoDetails = userdetails
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func registerTableViewCells() {
        let MyCell = UINib(nibName: "UserViewCell", bundle: nil)
        self.tableView.register(MyCell, forCellReuseIdentifier: "UserCell")
    }

//    func didPassedData(Response: UserInfo) {
//        userinfo?.total_count = 0
//        userinfo?.items?.removeAll()
//        userinfo = Response
//        if userinfo?.total_count == 0
//        {
//           presentAlert()
//        }else{
//            reloadData()
//        }
//    }
    
//
//    func reloadData(){
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
//    }
//
    func presentAlert(){
        
        let alert = UIAlertController(title: "No Results Found", message: "There are no such records", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { [self] action in
            //apiServices.fetchData(myparameter: #""""#)
            apiServices.fetchDataOfUsers2(searchBarText: #""""#) { userInfo in
                
                self.userinfo = userInfo
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        alert.addAction(action)
        DispatchQueue.main.async{
            self.present(alert, animated: true, completion: nil)
        }
       
        
    }
}
extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

