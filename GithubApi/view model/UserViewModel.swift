
import Foundation

//protocol DataPassingFromVMtoVC{
//    func didPassedData(Response : UserInfo)
//}

struct UserViewModel {
    //let githubUrl = "https://api.github.com/search/users?page=2&per_page=20&q="
//    var delegate : DataPassingFromVMtoVC?
//    mutating func fetchData(myparameter : String){
//        let original = githubUrl  + myparameter
//        if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
//           let url = URL(string: encoded)
//        {
//            fetchDataOfUsers(UrlString: url)
//        }
//    }
//
//    func fetchDataOfUsers(UrlString: URL?){
//        guard UrlString != nil else {
//            return
//        }
//        let session = URLSession(configuration: .default)
//
//        let task = session.dataTask(with: UrlString!) { (data, responce, error) in
//            if error != nil{
//                print(error?.localizedDescription ?? "error")
//                return
//            }
//
//            if let safeData = data{
//                let decoder = JSONDecoder()
//                do{
//                    let UserResponseData = try decoder.decode(UserInfo.self, from: safeData)
//                    self.delegate?.didPassedData(Response: UserResponseData)
//                }catch{
//                    print(error)
//                }
//            }
//        }
//        task.resume()
//    }
    
    func fetchDataOfUsers2(searchBarText: String, completion : @escaping(UserInfo)->()){
        
        let urlString = "https://api.github.com/search/users?q=\(searchBarText)&page=2&per_page=20"
        let encoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)

        guard let url = URL(string: encoded!) else {
            return
        }
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: url) { (data, responce, error) in
            if error != nil{
                print(error?.localizedDescription ?? "error")
                return
            }
            
            if let safeData = data{
                let decoder = JSONDecoder()
                do{
                    let userResponseData = try decoder.decode(UserInfo.self, from: safeData)
                    completion(userResponseData)
                }catch{
                    print(error)
                }
            }
        }
        task.resume()

    }


//    func fetchDataOfUsers(UrlString: String){
//        guard UrlString != nil else {
//            return
//        }
//        let url = URL(string: UrlString)
//        let session = URLSession(configuration: .default)
//
//        let task = session.dataTask(with: UrlString!) { (data, responce, error) in
//            if error != nil{
//                print(error?.localizedDescription ?? "error")
//                return
//            }
//
//            if let safeData = data{
//                let decoder = JSONDecoder()
//                do{
//                    let UserResponseData = try decoder.decode(UserInfo.self, from: safeData)
//                    self.delegate?.didPassedData(Response: UserResponseData)
//                }catch{
//                    print(error)
//                }
//            }
//        }
//        task.resume()
//    }
    
    
    func fetchDataForRepoResponse(urlString: String, completion : @escaping([UserDetails])->()){
        guard let url = URL(string: urlString) else{
            return
        }
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: url) { (data, responce, error) in
            if error != nil{
                print(error?.localizedDescription ?? "error")
                return
            }
            
            if let safeData = data{
                let decoder = JSONDecoder()
                do{
                    let UserResponseData2 = try decoder.decode([UserDetails].self, from: safeData)
                    completion(UserResponseData2)
                    
                }catch{
                    print(error)
                }
            }
        }
        task.resume()
    }
}

