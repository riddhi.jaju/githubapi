//
//  UserDetails.swift
//  GithubApi
//
//  Created by Coditas on 10/20/22.
//

import Foundation

struct UserDetails : Decodable{
    let name : String?
    let description : String?
    let created_at : String?
    let language : String?
}
