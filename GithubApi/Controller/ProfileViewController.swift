
import UIKit

class ProfileViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var profileimg: UIImageView!
    @IBOutlet weak var NameLbl: UILabel!
    
    var avatarStr : String? = ""
    var loginName : String? = ""
    var userRepoDetails : UserDetails?
    var repo : String?
    var apiServices = UserViewModel()
    var myAlert: UIAlertController = UIAlertController()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        myAlert = self.loadinHubShow()
        self.present(myAlert,animated: false, completion: nil)
        if let repoUrl = repo{
            self.apiServices.fetchDataForRepoResponse(urlString: repoUrl) {data in
                self.userRepoDetails = data[0]
                DispatchQueue.main.async {
                    self.tableview.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.loadinHubDismiss(alert: self.myAlert)
                    }
                }
            }
        }
        
        setUpUI()
        registerTableViewCells()
    }
    
    
    func setUpUI(){
        NameLbl.text = loginName
        profileimg.layer.borderWidth = 1.0
        profileimg.layer.masksToBounds = false
        profileimg.layer.borderColor = UIColor.white.cgColor
        profileimg.layer.cornerRadius = 35
        profileimg.clipsToBounds = true
        if let img = URL(string: avatarStr!){
            profileimg.load(url: img)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell",
                                                 for: indexPath) as! ProfileViewCell
        cell.loginDetailDescription.numberOfLines = 0
        cell.loginDetailDescription.lineBreakMode = .byWordWrapping
        cell.loginDetailDescription.preferredMaxLayoutWidth = 300
        cell.loginDetailDescription.sizeToFit()
        
        switch indexPath.row{
        case 0:
            cell.loginDetailHeadings.text = "Repository Name : "
            cell.myimg.image = UIImage(systemName: "square.stack.3d.up.fill")?.withTintColor(UIColor.black, renderingMode: UIImage.RenderingMode.alwaysOriginal)
            if let meranaam =  userRepoDetails?.name{
                cell.loginDetailDescription.text = meranaam
            }else{
                cell.loginDetailDescription.text = "nil"
            }
            
        case 1:
            cell.loginDetailHeadings.text = "Repository Description : "
            cell.myimg.image = UIImage(systemName: "folder.fill")?.withTintColor(UIColor.black, renderingMode: UIImage.RenderingMode.alwaysOriginal)
            if let meranaam1 =  userRepoDetails?.description{
                cell.loginDetailDescription.text = meranaam1
            }else{
                cell.loginDetailDescription.text = "nil"
            }
        case 2:
            cell.loginDetailHeadings.text = "Repository URL : "
            cell.myimg.image = UIImage(systemName: "link.circle.fill")?.withTintColor(UIColor.black, renderingMode: UIImage.RenderingMode.alwaysOriginal)
            if let meranaam8 =  repo{
                cell.loginDetailDescription.text = meranaam8
                cell.loginDetailDescription.textColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.8392156863, alpha: 1)
            }else{
                cell.loginDetailDescription.text = "nil"
            }
        case 3:
            cell.loginDetailHeadings.text = "Language : "
            cell.myimg.image = UIImage(systemName: "display")?.withTintColor(UIColor.black, renderingMode: UIImage.RenderingMode.alwaysOriginal)
            if let meranaam2 =  userRepoDetails?.language{
                cell.loginDetailDescription.text = meranaam2
            }else{
                cell.loginDetailDescription.text = "nil"
            }
        default:
            cell.loginDetailHeadings.text = "Created On : "
            cell.myimg.image = UIImage(systemName: "calendar")?.withTintColor(UIColor.black, renderingMode: UIImage.RenderingMode.alwaysOriginal)
            if let meranaam3 =  userRepoDetails?.created_at{
                let components = meranaam3.components(separatedBy: "T")
                cell.loginDetailDescription.text = components[0]
            }else{
                cell.loginDetailDescription.text = "nil"
            }
        }
        
        return cell
    }
    
    func registerTableViewCells() {
        let MyCell = UINib(nibName: "ProfileViewCell", bundle: nil)
        self.tableview.register(MyCell, forCellReuseIdentifier: "ProfileCell")
    }
    

}
extension ProfileViewController {
    
    func loadinHubShow()-> UIAlertController  {
        
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
        return alert
        //present(alert, animated: true, completion: nil)
    }
    
    func loadinHubDismiss(alert: UIAlertController){
        alert.dismiss(animated: false, completion: nil)
    }
}


