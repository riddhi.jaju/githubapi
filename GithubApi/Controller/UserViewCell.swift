//
//  UserViewCell.swift
//  GithubApi
//
//  Created by Coditas on 10/19/22.
//

import UIKit

class UserViewCell: UITableViewCell {

    @IBOutlet weak var imgOutlet: UIImageView!
    @IBOutlet weak var nameoutlet: UILabel!
    @IBOutlet weak var scoreoutlet: UILabel!
    @IBOutlet weak var viewDetailButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
