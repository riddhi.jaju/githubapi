import UIKit

class FirstViewController: UIViewController , UITableViewDelegate , UITableViewDataSource, UISearchBarDelegate{
    var apiServices = UserViewModel()
    var userinfo : UserInfo?        // Initial API response
    var userdetails : UserDetails?  // Repo url response
    var searchItem = ""
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchViewHtConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        registerTableViewCells()
        searchViewHtConstraint.constant = 0
        //tableView.tableFooterView = UIView()
        apiServices.fetchDataOfUsers2(searchBarText: #""""#) { userInfo in
            self.userinfo = userInfo
            DispatchQueue.main.async {
                self.tableView.tableFooterView = UIView()
                self.tableView.reloadData()
            }
        }
        
    }
    
    @IBAction func searchClicked(_ sender: Any) {
        if searchViewHtConstraint.constant == 0{
            searchViewHtConstraint.constant = 60
        }
        else{
            searchViewHtConstraint.constant = 0
        }

    }
    
    func registerTableViewCells() {
        let MyCell = UINib(nibName: "UserViewCell", bundle: nil)
        self.tableView.register(MyCell, forCellReuseIdentifier: "UserCell")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userinfo?.total_count == nil{
            return 0
        }else{
            return userinfo?.items?.count ?? 1
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text?.isEmpty == true {
            apiServices.fetchDataOfUsers2(searchBarText: #""""#) { userInfo in
                self.userinfo = userInfo
                //DispatchQueue.main.async {
                   
                //}
            }
            self.tableView.tableFooterView = UIView()
            self.tableView.reloadData()
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //self.userinfo?.items?.removeAll()
        self.apiServices.fetchDataOfUsers2(searchBarText: searchText) { userInfo in
            self.userinfo = userInfo
            if !searchText.isEmpty && self.userinfo?.items?.isEmpty == true{
                self.userinfo?.items?.removeAll()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                self.presentAlert()
            }
            else{
                self.userinfo = userInfo
                
            }
        }
        self.tableView.reloadData()

    }
    
    
    func presentAlert(){
        
        let alert = UIAlertController(title: "No Results Found", message: "There are no such records", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { [self] action in
            //apiServices.fetchData(myparameter: #""""#)
            apiServices.fetchDataOfUsers2(searchBarText: #""""#) { userInfo in
                
                self.userinfo = userInfo
//                DispatchQueue.main.async {
//                    tableView.tableFooterView = UIView()
//                    self.tableView.reloadData()
//                }
            }
        }
        alert.addAction(action)
        DispatchQueue.main.async{
            self.present(alert, animated: true, completion: nil)
        }
       
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell",
                                                 for: indexPath) as! UserViewCell
       
        cell.viewDetailButton.isHidden = false
        cell.imgOutlet.layer.borderWidth = 1.0
        cell.imgOutlet.layer.masksToBounds = false
        cell.imgOutlet.layer.borderColor = UIColor.white.cgColor
        cell.imgOutlet.layer.cornerRadius = 35
        cell.imgOutlet.clipsToBounds = true
        
        
        if let safename = userinfo?.items?[indexPath.row].login {
            cell.nameoutlet.text = safename
        }
        
        if let safescore = userinfo?.items?[indexPath.row].score{
            cell.scoreoutlet.text = "Score : \(safescore)"
        }
        
        if let safeimg = userinfo?.items?[indexPath.row].avatar_url{
            var safemyimg = URL(string: safeimg) ?? URL(string: "https://avatars.githubusercontent.com/u/499550?v=4")
            cell.imgOutlet.load(url: safemyimg!)
        }
        
        cell.viewDetailButton.tag = indexPath.row
        cell.viewDetailButton.addTarget(self, action: #selector(ViewDetailClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    
    @objc func ViewDetailClicked(_ sender: UIButton){
        guard let repoUrl = userinfo?.items?[sender.tag].repos_url else{
            return
        }
        if let vc = storyboard?.instantiateViewController(identifier: "Profile") as? ProfileViewController{
            vc.loginName = userinfo?.items![sender.tag].login
            if let data = userinfo?.items?[sender.tag].avatar_url{
                vc.avatarStr = data
            }
            vc.repo = repoUrl
            vc.userRepoDetails = userdetails
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
