//
//  UserInfo.swift
//  GithubApi
//
//  Created by Coditas on 10/19/22.
//

import Foundation

struct UserInfo : Decodable{
    var total_count : Int?
    var items : [Items]?
}

struct Items : Decodable {
    var login : String?
    var avatar_url : String?
    var repos_url : String?
    var score : Int?

}
