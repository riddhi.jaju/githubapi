//
//  ProfileViewCell.swift
//  GithubApi
//
//  Created by Coditas on 10/19/22.
//

import UIKit

class ProfileViewCell: UITableViewCell {

    @IBOutlet weak var myimg: UIImageView!
    @IBOutlet weak var loginDetailHeadings: UILabel!
    @IBOutlet weak var loginDetailDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
